#ifndef Defines_H
#define Defines_H

// Epidemic spreading
#define SUSCEPTIBLE 0
#define INFECTED 1
#define RECOVERED 2

// Rumor spreading
#define IGNORANT 0
#define SPREADER 1
#define STIFLER 2

#endif

# SpreadingMultilayer

SpreadingMultilayer,  (C) 2020  Guilherme Ferraz de Arruda \
SpreadingMultilayer comes with ABSOLUTELY NO WARRANTY; \
This iCopyrights free software, and you are welcome to redistribute it under certain conditions.

## Please cite:
&ensp;&ensp;[1] G. F. de Arruda, F. A. Rodrigues, and Y. Moreno, Physics Reports 756, 1 (2018).\
&ensp;&ensp;&ensp;https://doi.org/10.1016/j.physrep.2018.06.007 \
&ensp;&ensp;[2] G. F. de Arruda, E. Cozzo, T. P. Peixoto, F. A. Rodrigues, and Y. Moreno, Phys. Rev. X 7, 011014 (2017).\
&ensp;&ensp;&ensp;https://doi.org/10.1103/PhysRevX.7.011014

&ensp;Complementary, for DTMC formalism, see also: \
&ensp;&ensp;[3] Guilherme Ferraz de Arruda, Francisco Aparecido Rodrigues, Pablo Martín Rodríguez, Emanuele Cozzo, Yamir Moreno, Journal of Complex Networks, Volume 6, Issue 2, April 2018, Pages 215--242.\
&ensp;&ensp;&ensp;https://doi.org/10.1093/comnet/cnx024 \
&ensp;&ensp;&ensp;Codes avaliable: https://gitlab.com/guifarruda/DTMC

## Depends on:
&ensp;&ensp;1. gsl (https://www.gnu.org/software/gsl/) \
&ensp;&ensp;2. OpenMP (http://www.openmp.org/) \
&ensp;&ensp;3. Cmake (https://cmake.org/)

## Examples:
Run the compile_run_examples.sh to run some examples \
For more, please see: spreadingmultilayer -help

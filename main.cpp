#include "ContinuousProcesses.h"
#include "Markov.h"
#include "MonteCarlo.h"
#include "QS.h"
#include "Help.h"

#include <string.h>
#include <iostream>
#include <fstream>

#include <unistd.h>


int main ( int argc, char **argv )
{
     time_t start, end;
     time ( &start );

     // Begin -- User interface
     // Output file name variable
     char fname[500];

     char *InputNetwork = NULL;
     char *InputFile = NULL;

     // Dynamical parameters
     double lambda = 0.0;
     double eta = 0.0;
     double delta = 0.0;
     double y0 = 0.0;
     double tmax = 0;
     
     // MK parameter
     double nu = 0.0;

     // Weibull parameter (standard \alpha = 1.0 - exponential)
     // Allows for non-Markovian simulations
     double alpha = 1.0;

     // QS parameters -- standard values
     long QS_tr = 1000;
     long QS_ta = 100;
     long QS_M = 25;

     double QS_pr = 0.01;

     // CTMC - single run specific variables
     char id[128];

     // DTMC specific parameters
     double gamma = 0.0;
     long Nexp = 100;

     // Boolean variables for input processing
     bool bool_InputNetwork = false;
     bool bool_InputFile = false;
     bool bool_lambda = false;
     bool bool_eta = false;
     bool bool_delta = false;
     bool bool_y0 = false;
     bool bool_tmax = false;
     
     bool bool_nu = false;

     bool bool_alpha = false;

     bool bool_QS_tr = false;
     bool bool_QS_ta = false;
     bool bool_QS_M = false;
     bool bool_QS_pr = false;

     bool bool_gamma = false;
     bool bool_Nexp = false;

     bool bool_id = false;

     // Extra parameters
     char Method = 0;
     char Process = 0;
     char Time = 0;
     char DTMC_Contact = 0;
     char QS_type = 0;
     char DTMC_type = 0;

     for ( long i=1; i<argc; i++ ) {
          if ( strcmp ( argv[i], "-network" ) == 0 ) {
               InputNetwork = argv[i+1];
               bool_InputNetwork = true;
               

          } else if ( strcmp ( argv[i], "-parameters" ) == 0 ) {
               InputFile = argv[i+1];
               bool_InputFile = true;

          } else if ( strcmp ( argv[i], "-lambda" ) == 0 ) {
               lambda = atof ( argv[i+1] );
               bool_lambda = true;

          } else if ( strcmp ( argv[i], "-delta" ) == 0 ) {
               delta = atof ( argv[i+1] );
               bool_delta = true;

          } else if ( strcmp ( argv[i], "-eta" ) == 0 ) {
               eta = atof ( argv[i+1] );
               bool_eta = true;

          } else if ( strcmp ( argv[i], "-y0" ) == 0 ) {
               y0 = atof ( argv[i+1] );
               bool_y0 = true;

          } else if ( strcmp ( argv[i], "-tmax" ) == 0 ) {
               tmax = atof ( argv[i+1] );
               bool_tmax = true;

          } else if ( strcmp ( argv[i], "-nu" ) == 0 ) {
               nu = atof ( argv[i+1] );
               bool_nu = true;

          } else if ( strcmp ( argv[i], "-alpha" ) == 0 ) {
               alpha = atof ( argv[i+1] );
               bool_alpha = true;

          } else if ( strcmp ( argv[i], "-QSta" ) == 0 ) {
               QS_ta = atoi ( argv[i+1] );
               bool_QS_ta = true;

          } else if ( strcmp ( argv[i], "-QStr" ) == 0 ) {
               QS_tr = atoi ( argv[i+1] );
               bool_QS_tr = true;

          } else if ( strcmp ( argv[i], "-QSp" ) == 0 ) {
               QS_pr = atof ( argv[i+1] );
               bool_QS_pr = true;

          } else if ( strcmp ( argv[i], "-QSm" ) == 0 ) {
               QS_M = atoi ( argv[i+1] );
               bool_QS_M = true;

          } else if ( strcmp ( argv[i], "-Id" ) == 0 ) {
               strcpy ( id,  argv[i+1] );
               bool_id = true;

          } else if ( strcmp ( argv[i], "-gamma" ) == 0 ) {
               gamma = atof ( argv[i+1] );
               bool_gamma = true;

          } else if ( strcmp ( argv[i], "-Nexp" ) == 0 ) {
               Nexp = atoi ( argv[i+1] );
               bool_Nexp = true;

          } else if ( strcmp ( argv[i], "-Time" ) == 0 ) {
               if ( strcmp ( argv[i+1], "CTMC" ) == 0 ) {
                    Time = 1;

               } else if ( strcmp ( argv[i+1], "DTMC" ) == 0 ) {
                    Time = 2;

               }

          } else if ( strcmp ( argv[i], "-Process" ) == 0 ) {
               if ( strcmp ( argv[i+1], "SIS" ) == 0 ) {
                    Process = 1;

               } else if ( strcmp ( argv[i+1], "SIR" ) == 0 ) {
                    Process = 2;

               } else if ( strcmp ( argv[i+1], "MK" ) == 0 ) {
                    Process = 3;

               } else if ( strcmp ( argv[i+1], "CP" ) == 0 ) {
                    Process = 4;

               }


          } else if ( strcmp ( argv[i], "-Method" ) == 0 ) {
               if ( strcmp ( argv[i+1], "QS" ) == 0 ) {
                    Method = 1;
                    
                    if ( strcmp ( argv[i+2], "lambda" ) == 0 ) {
                         QS_type = 1;
                         
                    } else if ( strcmp ( argv[i+2], "eta" ) == 0 ) {
                         QS_type = 2;
                         
                    }

               } else if ( strcmp ( argv[i+1], "SingleRun" ) == 0 ) {
                    Method = 2;

               } else if ( strcmp ( argv[i+1], "SyncMarkov" ) == 0 ) {
                    Method = 3;

               } else if ( strcmp ( argv[i+1], "SyncMC" ) == 0 ) {
                    Method = 4;

               }

          } else if ( strcmp ( argv[i], "-Contacts" ) == 0 ) {
               if ( strcmp ( argv[i+1], "CP" ) == 0 ) {
                    DTMC_Contact = 1;

               } else if ( strcmp ( argv[i+1], "RP" ) == 0 ) {
                    DTMC_Contact = 2;

               }
               
          } else if ( strcmp ( argv[i], "-Reinfection" ) == 0 ) {
               DTMC_type = 1;
               
          } else if (strcmp ( argv[i], "-help" ) == 0){
               PrintHelp();
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
               
               return 0;
               
          } else if (strcmp ( argv[i], "-refs" ) == 0){
               PrintRefs();
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
     
               return 0;
               
          }

     }

     // Checking method incompatibilities
     if ( bool_InputNetwork == false ){
          cout << "Input Multilayer does not defeined." << endl;
          
          time ( &end );
          cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
          
          return 0;
          
     }
     
     if ( Method == 1 ) {
          if ( Process != 1 && Process != 4 ) {
               cout << "The QS method only applies to non-absorbing processes." << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
     
               return 0;

          }

          if ( Time != 1 ) {
               cout << "Only the Continuous-Time version of the QS Method was implemented." << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
               
               return 0;

          }
     }

     if ( DTMC_Contact > 0 && Time != 2 ) {
          cout << "The CP/RP contacts are only used in the discrete-time (DTMC) formalism." << endl;

     }

     if (Time == 1 && DTMC_type == 1){
          cout << "Reinfections are only possible in the discrete-time (DTMC) formalism." << endl;
          
     }
     
     if (Time == 1 && bool_alpha == false){
          cout << "\\alpha = 1.0, therefore a Markovian Process." << endl;
          
     }
     
     if (Method == 1 && (bool_QS_tr == false || bool_QS_ta == false || bool_QS_M == false || bool_QS_pr == false) ){
          cout << "Some of the QS parameters are not set, thus standard values are used." << endl;
          
     }
     
     // Defining some standards, if not previously defined
     if ( bool_id == false ) {
          sprintf ( id, "0" );
     }
     // End -- User interface

     
     // Begin -- Initializing basic structures
     // Multilayer initialization
     Multilayer M ( 2 );

     cout << "Multilayer -- UnDirected:" << endl;
     M.ReadEdgelist ( InputNetwork );
     M.DegreeCentrality();

     cout << "Multilayer -- Structure:" << endl;
     cout << "N = " << M.N << endl;
     cout << "M = " << M.M << endl;
     cout << "E = " << M.E << endl;
     cout << "E_inter = " << M.interE << endl;
     cout << endl;
     
     if (M.N <= 0 || M.E <= 0){
          cout << "The Input Multilayer is not well defined." << endl;
          
          time ( &end );
          cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
     
          return 0;
          
     }

     // Random number generator initialization
     const gsl_rng_type * Tr;
     gsl_rng * randomic;
     Tr = gsl_rng_default;
     randomic = gsl_rng_alloc ( Tr );
     gsl_rng_set ( randomic, time ( NULL ) *getpid() );
     std::srand ( unsigned ( std::time ( 0 ) ) *getpid() );
     // End -- Initializing basic structures

     
     // Begin Continuous-time processes simulations (CTMC) -- single run
     if ( Time == 1 && Method == 2 ) {
          if (bool_lambda == false || bool_delta == false || bool_eta == false || bool_y0 == false || bool_tmax == false ){
               cout << "Parameters are missing." << endl;
               cout << "Please check: " << argv[0] << " -help" << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
               
               return 0;
               
          }
          
          if ( Process == 1 ) {
               cout << "Single Run SIS dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "\\alpha = " << alpha << endl;
               cout << "tmax = " << tmax << endl;
               cout << "y_0 = " << y0 << endl;

               sprintf ( fname, "SingleRun_SIS_%s_lb_%.6f_eta_%.6f_delta_%.6f_tmax_%.6f_alpha_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, lambda, eta, delta, tmax, alpha, y0, id );

               ContinuousProcesses_SIS ( M, tmax, y0, lambda, eta, delta, randomic, fname );

          } else if ( Process == 2 ) {
               cout << "Single Run SIR dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "\\alpha = " << alpha << endl;
               cout << "tmax = " << tmax << endl;
               cout << "y_0 = " << y0 << endl;

               sprintf ( fname, "SingleRun_SIR_%s_lb_%.6f_eta_%.6f_delta_%.6f_tmax_%.6f_alpha_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, lambda, eta, delta, tmax, alpha, y0, id );

               ContinuousProcesses_SIR ( M, tmax, y0, lambda, eta, delta, randomic, fname );

          } else if ( Process == 3 ) {
               if (bool_nu == false ){
                    cout << "Parameters are missing." << endl;
                    cout << "Please check: " << argv[0] << " -help" << endl;
                    
                    time ( &end );
                    cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
                    
                    return 0;
                    
               }
               
               cout << "Single Run Maki-Thompson (MK) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\nu = " << nu << endl;
               cout << "\\delta = " << delta << endl;
               cout << "\\alpha = " << alpha << endl;
               cout << "tmax = " << tmax << endl;
               cout << "y_0 = " << y0 << endl;

               sprintf ( fname, "SingleRun_MK_%s_lb_%.6f_eta_%.6f_nu_%.6f_delta_%.6f_tmax_%.6f_alpha_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, lambda, eta, nu, delta, tmax, alpha, y0, id );

               ContinuousProcesses_MK ( M, tmax, y0, lambda, eta, delta, nu, randomic, fname );

          } else{
               cout << "This process was not implemented." << endl;
               cout << "The implemented processes for the single-run CTMC simulations are: SIS, SIR and MK." << endl;
               return 0;
               
          }

     // SIS - varying Lambda
     // Continuous-time processes simulations (CTMC) -- QS method: higher statistics of the phase diagram
     } else if ( Time == 1 && Method == 1 && QS_type == 1 && Process == 1 ) {
          if ( bool_delta == false || bool_eta == false || bool_y0 == false || bool_InputFile == false ){
               cout << "Parameters are missing." << endl;
               cout << "Please check: " << argv[0] << " -help" << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
     
               return 0;
               
          }
          
          cout << endl;
          cout << "QS Adaptative method parameters:" << endl;
          cout << "Lambda file: " << InputFile << endl;
          cout << "t_r = " << QS_tr << endl;
          cout << "t_a = " << QS_ta << endl;
          cout << "M = " << QS_M << endl;
          cout << "QS(p) = " << QS_pr << endl;

          cout << endl;

          cout << "General SIS Process QS method dynamics -- Parameters:" << endl;
          cout << "\\eta = " << eta << endl;
          cout << "\\delta = " << delta << endl;
          cout << "\\alpha = " << alpha << endl;

          cout << "Distribution -- Parameters:" << endl;

          vector <double> Lambda;

          FILE *fp_lambda = fopen ( InputFile, "r" );
          long lambdaF = 0;
          fscanf ( fp_lambda, "%ld", &lambdaF );

          for ( long lambdaX = 0; lambdaX<lambdaF; lambdaX++ ) {
               double lambdat = 0.0;
               fscanf ( fp_lambda, "%lf", &lambdat );
               Lambda.insert ( Lambda.begin(), lambdat );
               
          }

          fclose ( fp_lambda );

          cout << "Lambda file: " << InputFile << endl;
          cout << "\\lambda = " << endl;
          for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
               cout << Lambda[i] << ",  ";
          }
          cout << endl;

          vector <double> X ( ( long ) Lambda.size(), -1 );
          vector <double> rho ( ( long ) Lambda.size(), -1 );

          vector <vector <double> > rho2 ( ( long ) Lambda.size() );
          vector <vector <double> > X2 ( ( long ) Lambda.size() );

          for ( long k=0; k< ( long ) Lambda.size(); k++ ) {
               rho2[k] = vector <double> ( M.M, -1 );
               X2[k] = vector <double> ( M.M, -1 );
          }

          sprintf ( fname, "QS_SIS_%s_LambdaFile_%s_eta_%.6f_delta_%.6f_tmax_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, InputFile, eta, delta, tmax, y0, id );

          
          if ( access ( fname, F_OK ) != -1 ) {
               printf ( "File exists, continuing ...\n" );

               // file exists
               FILE *fp = fopen ( fname, "r" );
               double b_b = 0, b_x = 0, b_r = 0;
               while ( fscanf ( fp, "%lf\t%lf\t%lf", &b_b, &b_x, &b_r ) == 3 ) { // expect 1 successful conversion

                    printf ( "\t%lf\t%lf\t%lf", b_b, b_x, b_r );
                    bool in_file = false;
                    for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
                         if ( b_b == Lambda[i] ) {
                              in_file = true;

                              X[i] = ( double ) b_x;
                              rho[i] = ( double ) b_r;

                              for ( long k=0; k<M.M; k++ ) {
                                   double rhok = 0.0, xk = 0.0;
                                   if ( fscanf ( fp, "\t%lf\t%lf", &rhok, &xk ) == 2 ) {
                                        printf ( "\t%lf\t%lf", rhok, xk );
                                        rho2[i][k] = ( double ) rhok;
                                        X2[i][k] = ( double ) xk;
                                        
                                   } else {
                                        printf ( "ERROR!!! Incompatible input file!" );
                                        return 1;
                                        
                                   }
                              }

                         }
                    }

                    if ( in_file == false ) {
                         for ( long k=0; k<M.M; k++ ) {
                              double rhok = 0.0, xk = 0.0;
                              if ( fscanf ( fp, "\t%lf\t%lf", &rhok, &xk ) == 2 ) {
                                   printf ( "\t%lf\t%lf", rhok, xk );
                                   
                              } else {
                                   printf ( "ERROR!!! Incompatible input file!" );
                                   return 1;
                                   
                              }
                         }
                    }

                    printf ( "\n" );
               }


               fclose ( fp );
          }

          FILE *fp = fopen ( fname, "a" );

          bool once = false;

          #pragma omp parallel for schedule(dynamic,1)
          for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
               if ( X[i] == -1 || rho[i] == -1 ) {
                    cout << "Running \\lambda = " << Lambda[i] << endl;
                    double r = 0.0, x = 0.0;
                    vector <double> r2 =vector <double> ( M.M, -1 );
                    vector <double> x2 =vector <double> ( M.M, -1 );

                    QS_Adaptative_GSIS ( M, y0, alpha, Lambda[i], eta*Lambda[i], delta, QS_pr, QS_tr, QS_ta, QS_M, randomic, x, r, x2, r2, fname );

                    X[i] = x;
                    rho[i] = r;
                    cout << "\\lambda = " << Lambda[i] << " Done! " << "X = " << X[i] << ", rho = " << rho[i];
                    cout.flush();

                    for ( long k=0; k<M.M; k++ ) {
                         rho2[i][k] = r2[k];
                         X2[i][k] = x2[k];

                         cout << " rho[" << k << "] = " << rho2[i][k] << " X[" << k << "] = " << X2[i][k];
                         cout.flush();
                         
                    }
                    cout << endl;

                    fprintf ( fp, "%e\t%e\t%e\t", Lambda[i], X[i], rho[i] );
                    for ( long k=0; k< ( long ) M.M; k++ ) {
                         if ( k != ( long ) M.M-1 ) {
                              fprintf ( fp, "%e\t%e\t", X2[i][k], rho2[i][k] );
                              
                         } else {
                              fprintf ( fp, "%e\t%e", X2[i][k], rho2[i][k] );
                              
                         }
                    }
                    fprintf ( fp, "\n" );
                    fflush ( fp );

                    once = true;
               }
          }

          fclose ( fp );

          if ( once ) {

               sprintf ( fname, "QS_SIS_%s_LambdaFile_%s_eta_%.6f_delta_%.6f_tmax_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, InputFile, eta, delta, tmax, y0, id );

               fp = fopen ( fname, "w" );

               for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
                    fprintf ( fp, "%e\t%e\t%e\n", Lambda[i], X[i], rho[i] );
               }

               fclose ( fp );
          }


     // SIS - varying Eta
     // Continuous-time processes simulations (CTMC) -- QS method: higher statistics of the phase diagram
     } else if ( Time == 1 && Method == 1 && QS_type == 2 && Process == 1 ) {
          if (bool_lambda == false || bool_delta == false || bool_y0 == false || bool_InputFile == false ){
               cout << "Parameters are missing." << endl;
               cout << "Please check: " << argv[0] << " -help" << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
     
               return 0;
               
          }
          
          cout << endl;
          cout << "QS Adaptative method parameters:" << endl;
          cout << "Eta file: " << InputFile << endl;
          cout << "tr = " << QS_tr << endl;
          cout << "ta = " << QS_ta << endl;
          cout << "M = " << QS_M << endl;
          cout << "pr = " << QS_pr << endl;

          cout << endl;

          cout << "General SIS Process QS method dynamics -- Parameters:" << endl;
          cout << "\\lambda = " << lambda << endl;
          cout << "\\eta = " << eta << endl;
          cout << "\\delta = " << delta << endl;
          cout << "\\alpha = " << alpha << endl;

          cout << "Distribution -- Parameters:" << endl;

          vector <double> Eta;

          FILE *fp_eta = fopen ( InputFile, "r" );
          long etaF = 0;
          fscanf ( fp_eta, "%ld", &etaF );

          for ( long etaX = 0; etaX<etaF; etaX++ ) {
               double etat = 0.0;
               fscanf ( fp_eta, "%lf", &etat );
               Eta.insert ( Eta.begin(), etat );
          }

          fclose ( fp_eta );

          cout << "Eta file: " << InputFile << endl;
          cout << "\\eta = " << endl;
          for ( int i=0; i< ( long ) Eta.size(); i++ ) {
               cout << Eta[i] << ", ";
          }
          cout << endl;

          vector <double> X ( ( long ) Eta.size(), -1 );
          vector <double> rho ( ( long ) Eta.size(), -1 );

          vector <vector <double> > rho2 ( ( long ) Eta.size() );
          vector <vector <double> > X2 ( ( long ) Eta.size() );

          for ( long k=0; k< ( long ) Eta.size(); k++ ) {
               rho2[k] = vector <double> ( M.M, -1 );
               X2[k] = vector <double> ( M.M, -1 );
               
          }

          sprintf ( fname, "QS_SIS_%s_EtaFile_%s_lambda_%.6f_delta_%.6f_tmax_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, InputFile, lambda, delta, tmax, y0, id );

          if ( access ( fname, F_OK ) != -1 ) {
               printf ( "File exists, continuing ...\n" );

               // file exists
               FILE *fp = fopen ( fname, "r" );
               double b_b = 0, b_x = 0, b_r = 0;
               
               while ( fscanf ( fp, "%lf\t%lf\t%lf", &b_b, &b_x, &b_r ) == 3 ) { // expect 1 successful conversion

                    printf ( "\t%lf\t%lf\t%lf", b_b, b_x, b_r );

                    for ( int i=0; i< ( long ) Eta.size(); i++ ) {
                         if ( b_b == Eta[i] ) {
                              X[i] = ( double ) b_x;
                              rho[i] = ( double ) b_r;

                              for ( long k=0; k<M.M; k++ ) {
                                   double rhok = 0.0, xk = 0.0;
                                   if ( fscanf ( fp, "\t%lf\t%lf", &rhok, &xk ) == 2 ) {
                                        printf ( "\t%lf\t%lf", rhok, xk );
                                        rho2[i][k] = ( double ) rhok;
                                        X2[i][k] = ( double ) xk;
                                        
                                   } else {
                                        printf ( "ERROR!!! Incompatible input file!" );
                                        return 1;
                                        
                                   }
                              }

                         }
                    }

                    printf ( "\n" );
               }
               fclose ( fp );
          }

          FILE *fp = fopen ( fname, "a" );

          bool once = false;

          #pragma omp parallel for schedule(dynamic,1)
          for ( int i=0; i< ( long ) Eta.size(); i++ ) {
               if ( X[i] == -1 || rho[i] == -1 ) {
                    cout << "Running \\eta = " << Eta[i] << endl;
                    double r = 0.0, x = 0.0;
                    vector <double> r2 =vector <double> ( M.M, -1 );
                    vector <double> x2 =vector <double> ( M.M, -1 );

                    QS_Adaptative_GSIS ( M, y0, alpha, lambda, Eta[i], delta, QS_pr, QS_tr, QS_ta, QS_M, randomic, x, r, x2, r2, fname );
                    
                    X[i] = x;
                    rho[i] = r;
                    cout << "eta = " << Eta[i] << " Done! " << "X = " << X[i] << ", rho = " << rho[i];
                    cout.flush();

                    for ( long k=0; k<M.M; k++ ) {
                         rho2[i][k] = r2[k];
                         X2[i][k] = x2[k];

                         cout << " rho[" << k << "] = " << rho2[i][k] << " X[" << k << "] = " << X2[i][k];
                         cout.flush();
                         
                    }
                    
                    cout << endl;

                    fprintf ( fp, "%e\t%e\t%e\t", Eta[i], X[i], rho[i] );
                    for ( long k=0; k< ( long ) M.M; k++ ) {
                         if ( k != ( long ) M.M-1 ) {
                              fprintf ( fp, "%e\t%e\t", X2[i][k], rho2[i][k] );
                              
                         } else {
                              fprintf ( fp, "%e\t%e", X2[i][k], rho2[i][k] );
                              
                         }
                    }
                    fprintf ( fp, "\n" );
                    fflush ( fp );

                    once = true;
               }
          }

          fclose ( fp );

          if ( once ) {

               sprintf ( fname, "QS_SIS_%s_EtaFile_%s_lambda_%.6f_delta_%.6f_tmax_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, InputFile, lambda, delta, tmax, y0, id );

               fp = fopen ( fname, "w" );

               for ( int i=0; i< ( long ) Eta.size(); i++ ) {
                    fprintf ( fp, "%e\t%e\t%e\n", Eta[i], X[i], rho[i] );
               }

               fclose ( fp );
          }

     
     // CP varying Lambda
     // Continuous-time processes simulations (CTMC) -- QS method: higher statistics of the phase diagram
     } else if ( Time == 1 && Method == 1 && QS_type == 1  && Process == 4 ) {
          if ( bool_delta == false || bool_eta == false || bool_y0 == false || bool_InputFile == false ){
               cout << "Parameters are missing." << endl;
               cout << "Please check: " << argv[0] << " -help" << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
     
               return 0;
               
          }
          
          cout << endl;
          cout << "QS Adaptative method parameters:" << endl;
          cout << "Lambda file: " << InputFile << endl;
          cout << "t_r = " << QS_tr << endl;
          cout << "t_a = " << QS_ta << endl;
          cout << "M = " << QS_M << endl;
          cout << "QS(p) = " << QS_pr << endl;

          cout << endl;

          cout << "General Contact Process (CP) QS method dynamics -- Parameters:" << endl;
          cout << "\\eta = " << eta << endl;
          cout << "\\delta = " << delta << endl;
          cout << "\\alpha = " << alpha << endl;

          cout << "Distribution -- Parameters:" << endl;

          vector <double> Lambda;

          FILE *fp_lambda = fopen ( InputFile, "r" );
          long lambdaF = 0;
          fscanf ( fp_lambda, "%ld", &lambdaF );

          for ( long lambdaX = 0; lambdaX<lambdaF; lambdaX++ ) {
               double lambdat = 0.0;
               fscanf ( fp_lambda, "%lf", &lambdat );
               Lambda.insert ( Lambda.begin(), lambdat );
               
          }

          fclose ( fp_lambda );

          cout << "Lambda file: " << InputFile << endl;
          cout << "\\lambda = " << endl;
          for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
               cout << Lambda[i] << ",  ";
               
          }
          cout << endl;

          vector <double> X ( ( long ) Lambda.size(), -1 );
          vector <double> rho ( ( long ) Lambda.size(), -1 );

          vector <vector <double> > rho2 ( ( long ) Lambda.size() );
          vector <vector <double> > X2 ( ( long ) Lambda.size() );

          for ( long k=0; k< ( long ) Lambda.size(); k++ ) {
               rho2[k] = vector <double> ( M.M, -1 );
               X2[k] = vector <double> ( M.M, -1 );
               
          }

          sprintf ( fname, "QS_CP_%s_LambdaFile_%s_eta_%.6f_delta_%.6f_tmax_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, InputFile, eta, delta, tmax, y0, id );

          
          if ( access ( fname, F_OK ) != -1 ) {
               printf ( "File exists, continuing ...\n" );

               // file exists
               FILE *fp = fopen ( fname, "r" );
               double b_b = 0, b_x = 0, b_r = 0;
               while ( fscanf ( fp, "%lf\t%lf\t%lf", &b_b, &b_x, &b_r ) == 3 ) { // expect 1 successful conversion

                    printf ( "\t%lf\t%lf\t%lf", b_b, b_x, b_r );
                    bool in_file = false;
                    for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
                         if ( b_b == Lambda[i] ) {
                              in_file = true;

                              X[i] = ( double ) b_x;
                              rho[i] = ( double ) b_r;

                              for ( long k=0; k<M.M; k++ ) {
                                   double rhok = 0.0, xk = 0.0;
                                   if ( fscanf ( fp, "\t%lf\t%lf", &rhok, &xk ) == 2 ) {
                                        printf ( "\t%lf\t%lf", rhok, xk );
                                        rho2[i][k] = ( double ) rhok;
                                        X2[i][k] = ( double ) xk;
                                        
                                   } else {
                                        printf ( "ERROR!!! Incompatible input file!" );
                                        return 1;
                                        
                                   }
                              }

                         }
                    }

                    if ( in_file == false ) {
                         for ( long k=0; k<M.M; k++ ) {
                              double rhok = 0.0, xk = 0.0;
                              if ( fscanf ( fp, "\t%lf\t%lf", &rhok, &xk ) == 2 ) {
                                   printf ( "\t%lf\t%lf", rhok, xk );
                                   
                              } else {
                                   printf ( "ERROR!!! Incompatible input file!" );
                                   return 1;
                                   
                              }
                         }
                    }

                    printf ( "\n" );
               }


               fclose ( fp );
          }

          FILE *fp = fopen ( fname, "a" );

          bool once = false;

          #pragma omp parallel for schedule(dynamic,1)
          for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
               if ( X[i] == -1 || rho[i] == -1 ) {
                    cout << "Running \\lambda = " << Lambda[i] << endl;
                    double r = 0.0, x = 0.0;
                    vector <double> r2 =vector <double> ( M.M, -1 );
                    vector <double> x2 =vector <double> ( M.M, -1 );

                    QS_Adaptative_GCP ( M, y0, alpha, Lambda[i], eta*Lambda[i], delta, QS_pr, QS_tr, QS_ta, QS_M, randomic, x, r, x2, r2, fname );

                    X[i] = x;
                    rho[i] = r;
                    cout << "\\lambda = " << Lambda[i] << " Done! " << "X = " << X[i] << ", rho = " << rho[i];
                    cout.flush();

                    for ( long k=0; k<M.M; k++ ) {
                         rho2[i][k] = r2[k];
                         X2[i][k] = x2[k];

                         cout << " rho[" << k << "] = " << rho2[i][k] << " X[" << k << "] = " << X2[i][k];
                         cout.flush();
                         
                    }
                    
                    cout << endl;

                    fprintf ( fp, "%e\t%e\t%e\t", Lambda[i], X[i], rho[i] );
                    for ( long k=0; k< ( long ) M.M; k++ ) {
                         if ( k != ( long ) M.M-1 ) {
                              fprintf ( fp, "%e\t%e\t", X2[i][k], rho2[i][k] );
                              
                         } else {
                              fprintf ( fp, "%e\t%e", X2[i][k], rho2[i][k] );
                              
                         }
                    }
                    fprintf ( fp, "\n" );
                    fflush ( fp );

                    once = true;
               }
          }

          fclose ( fp );

          if ( once ) {

               sprintf ( fname, "QS_CP_%s_LambdaFile_%s_eta_%.6f_delta_%.6f_tmax_%.6f_y0_%.6f_ID_%s.txt", InputNetwork, InputFile, eta, delta, tmax, y0, id );

               fp = fopen ( fname, "w" );

               for ( int i=0; i< ( long ) Lambda.size(); i++ ) {
                    fprintf ( fp, "%e\t%e\t%e\n", Lambda[i], X[i], rho[i] );
                    
               }

               fclose ( fp );
          }
          
         
     
     // DTMC Monte Carlo simulations (synchronous cellular automata with discrete-time)
     } else if ( Time == 2 && Method == 4 ) {
          if (bool_lambda == false || bool_delta == false || bool_eta == false || bool_y0 == false || DTMC_Contact == 0 ){
               cout << "Parameters are missing." << endl;
               cout << "Please check: " << argv[0] << " -help" << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
               
               return 0;
               
          }
          
          if (bool_Nexp == false){
               cout << "Nexp not set, using the standard value." << endl;
               
          }
          
          long Steps = ceil(tmax);

          if ( Process == 1 && DTMC_Contact == 2 && DTMC_type == 0 ) {
               cout << "Monte Carlo SIS-RP without reinfections (synchronous) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "y_0 = " << y0 << endl;
               cout << "T_max = " << Steps << endl;
               cout << "Nexp = " << Nexp << endl;
               cout << endl;

               sprintf ( fname, "CA_MonteCarlo_SIS-RP_%s_lambda_%.6f_eta_%.6f_delta_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, y0 );
               MonteCarlo_Aux ( M, Nexp, y0, Steps, lambda, eta, delta, randomic, 0, fname );

               
          } else if ( Process == 1 && DTMC_Contact == 2 && DTMC_type == 1 ) {
               cout << "Monte Carlo SIS-RP with reinfections (synchronous) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "y_0 = " << y0 << endl;
               cout << "T_max = " << Steps << endl;
               cout << "Nexp = " << Nexp << endl;
               cout << endl;

               sprintf ( fname, "CA_MonteCarlo_SIS-RP_w_reinfection_%s_lambda_%.6f_eta_%.6f_delta_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, y0 );
               MonteCarlo_Aux ( M, Nexp, y0, Steps, lambda, eta, delta, randomic, 1, fname );

               
          } else if ( Process == 2 && DTMC_Contact == 2 ) {
               cout << "Monte Carlo SIR-RP without reinfections (synchronous) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "y_0 = " << y0 << endl;
               cout << "T_max = " << Steps << endl;
               cout << "Nexp = " << Nexp << endl;
               cout << endl;

               sprintf ( fname, "CA_MonteCarlo_SIR-RP_w_reinfection_%s_lambda_%.6f_eta_%.6f_delta_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, y0 );
               MonteCarlo_Aux ( M, Nexp, y0, Steps, lambda, eta, delta, randomic, 2, fname );

               
          } else if ( Process == 1 && DTMC_Contact == 1 && DTMC_type == 0 ) {
               cout << "Monte Carlo SIS-CP without reinfections (synchronous) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "y_0 = " << y0 << endl;
               cout << "T_max = " << Steps << endl;
               cout << "Nexp = " << Nexp << endl;
               cout << endl;

               sprintf ( fname, "CA_MonteCarlo_SIS-CP_%s_lambda_%.6f_eta_%.6f_delta_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, y0 );
               MonteCarlo_Aux ( M, Nexp, y0, Steps, lambda, eta, delta, randomic, 3, fname );

               
          } else if ( Process == 1 && DTMC_Contact == 1 && DTMC_type == 1 ) {
               cout << "Monte Carlo SIS-CP with reinfections (synchronous) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "y_0 = " << y0 << endl;
               cout << "T_max = " << Steps << endl;
               cout << "Nexp = " << Nexp << endl;
               cout << endl;

               sprintf ( fname, "CA_MonteCarlo_SIS-CP_w_reinfection_%s_lambda_%.6f_eta_%.6f_delta_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, y0 );
               MonteCarlo_Aux ( M, Nexp, y0, Steps, lambda, eta, delta, randomic, 4, fname );
               

          } else if ( Process == 2 && DTMC_Contact == 1 ) {
               cout << "Monte Carlo SIR-CP without reinfections (synchronous) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "y_0 = " << y0 << endl;
               cout << "T_max = " << Steps << endl;
               cout << "Nexp = " << Nexp << endl;
               cout << endl;

               sprintf ( fname, "CA_MonteCarlo_SIR-CP_w_reinfection_%s_lambda_%.6f_eta_%.6f_delta_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, y0 );
               MonteCarlo_Aux ( M, Nexp, y0, Steps, lambda, eta, delta, randomic, 5, fname );

               
          }

          
     // Begin Markov iteration calculations
     } else if ( Time == 2 && Method == 3 ) {
          if (bool_lambda == false || bool_delta == false || bool_eta == false || bool_y0 == false  || bool_gamma == false ){
               cout << "Parameters are missing." << endl;
               cout << "Please check: " << argv[0] << " -help" << endl;
               
               time ( &end );
               cout << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;
               
               return 0;
               
          }
          
          long Steps = ceil(tmax);
          

          if ( Process == 1 && DTMC_type == 0) {
               cout << "Markov SIS (without reinfection) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "\\gamma [activity] = " << gamma << endl;
               cout << endl;

               double **p = Markov_SIS ( M, Steps, y0, lambda, delta, eta, gamma );
               
               sprintf ( fname, "CA_Markov_Individual_SIS_%s_lambda_%.6f_eta_%.6f_delta_%.6f_gamma_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, gamma, y0 );
               FILE *fpf = fopen ( fname, "w" );
               printf ( "File nodal probabilities:\t%s\n", fname );

               sprintf ( fname, "CA_Markov_SIS_%s_lambda_%.6f_eta_%.6f_delta_%.6f_gamma_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, gamma, y0 );
               FILE *fp_out = fopen ( fname, "w" );
               printf ( "File:\t\t\t\t%s\n\n", fname );

               Markov_SIS_PostProcessing ( M, Steps, p, fpf, fp_out );


          } else if ( Process == 1 && DTMC_type == 1 ) {
               cout << "Markov SIS (with reinfection) dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "\\gamma [activity] = " << gamma << endl;
               cout << endl;

               double **p = Markov_SIS_Reinfection ( M, Steps, y0, lambda, delta, eta, gamma );
               
               sprintf ( fname, "CA_Markov_Individual_SIS_reinfection_%s_lambda_%.6f_eta_%.6f_delta_%.6f_gamma_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, gamma, y0 );
               FILE *fpf = fopen ( fname, "w" );
               printf ( "File nodal probabilities:\t%s\n", fname );

               sprintf ( fname, "CA_Markov_SIS_reinfection_%s_lambda_%.6f_eta_%.6f_delta_%.6f_gamma_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, gamma, y0 );
               FILE *fp_out = fopen ( fname, "w" );
               printf ( "File:\t\t\t\t%s\n\n", fname );

               Markov_SIS_PostProcessing ( M, Steps, p, fpf, fp_out );


          } else if ( Process == 2 ) {
               cout << "Markov SIR dynamics -- Parameters:" << endl;
               cout << "\\lambda = " << lambda << endl;
               cout << "\\eta = " << eta << endl;
               cout << "\\delta = " << delta << endl;
               cout << "\\gamma [activity] = " << gamma << endl;
               cout << endl;

               sir p = Markov_SIR ( M, Steps, y0, lambda, delta, eta, gamma );
               
               sprintf ( fname, "CA_Markov_Individual_SIR_%s_lambda_%.6f_eta_%.6f_delta_%.6f_gamma_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, gamma, y0 );
               FILE *fpf = fopen ( fname, "w" );
               printf ( "File nodal probabilities:\t%s\n", fname );

               sprintf ( fname, "CA_Markov_SIR_%s_lambda_%.6f_eta_%.6f_delta_%.6f_gamma_%.6f_y0_%.6f.txt", InputNetwork, lambda, eta, delta, gamma, y0 );
               FILE *fp_out = fopen ( fname, "w" );
               printf ( "File:\t\t\t\t%s\n\n", fname );

               Markov_SIR_PostProcessing ( M, Steps, p, fpf, fp_out );

          }
          
          
     // End Markov processes calculations
     } else{
          cout << "Parameters are missing." << endl;
          cout << "Please check: " << argv[0] << " -help" << endl;
          
     }
     

     time ( &end );
     cout << endl << "Finished in: " << difftime ( end, start ) << " seconds" <<endl;

     return 0;
}

#include "Help.h" 


int PrintRefs(){
     cout << "Please cite: " << endl;
     cout << "  [1] G. F. de Arruda, F. A. Rodrigues, and Y. Moreno, Physics Reports 756, 1 (2018)." << endl;
     cout << "      https://doi.org/10.1016/j.physrep.2018.06.007" << endl;
     
     cout << "  [2] G. F. de Arruda, E. Cozzo, T. P. Peixoto, F. A. Rodrigues, and Y. Moreno, Phys. Rev. X 7, 011014 (2017)." << endl;
     cout << "      https://doi.org/10.1103/PhysRevX.7.011014" << endl;

     cout << endl << "Complementary, for DTMC formalism, see also:" << endl;
     cout << "  [3] Guilherme Ferraz de Arruda, Francisco Aparecido Rodrigues, Pablo Martín Rodríguez, Emanuele Cozzo, Yamir Moreno," << endl;
     cout << "      Journal of Complex Networks, Volume 6, Issue 2, April 2018, Pages 215–242." << endl;
     cout << "      https://doi.org/10.1093/comnet/cnx024" << endl;
     cout << "      Codes avaliable: https://gitlab.com/guifarruda/DTMC" << endl;
          
     return 0;
}

int PrintExtra(){
     cout << "The source code can be found at: https://gitlab.com/guifarruda/spreadingmultilayer" << endl;
     
     return 0;
}


int PrintHelp(){

     cout  << "spreadingmultilayer <parameters>" << endl;
     cout << endl << "Parameters:" << endl;
     
     cout << endl << " Multilayer definition:" << endl;
     cout << "   -network <file name>" << endl;
     cout << "    Network File Format:" << endl;
     cout << "      [Number of nodes] [Number of layers] [Number of edges]" << endl;
     cout << "      [NodeId i] [NodeId j] [LayerId i] [LayerId j]" << endl;
     
     cout << endl << " Implemented methods and simulations:" << endl;
     cout << "   -Time <CTMC / DTMC> \t\t\t\t\t\t-- [CTMC] Continuous or [DTMC] Discrete-time (Synchronous Cellular Automata) simulations." << endl;
     cout << "   -Process <SIS / SIR / MK / CP> \t\t\t\t-- The processes can be: SIS, SIR, MK (Maki-Thompson rumor spreading) and CP (contact process)." << endl;
     cout << "   -Method <QS [lambda/eta] / SingleRun / SyncMarkov / SyncMC> \t-- Define the method: (1) QS [lambda/eta]: Quasi-Stationary method that can be as a function of lambda or eta; (2) SingleRun: a single run over time;" << endl;
     cout << "\t\t\t\t\t\t\t\t   (3) SyncMarkov: Run the Markov iterative equations for the DTMC Synchronous Cellular Automata; (4) SyncMC: Run the Monte Carlo simulations for the" << endl; 
     cout << "\t\t\t\t\t\t\t\t   DTMC Synchronous Cellular Automata. Options 1 and 2 are defined for CTMC, while 3 and 4 for DTMC. Options 3 and 4 are complementary." << endl;
     cout << "\t\t\t\t\t\t\t\t   Note: When \"-Method QS lambda\" is used, the parameter -eta is assumed to be (\\eta)/(\\lambda), for more, see Refs. [1-2]." << endl;
     cout << "   -Contacts <CP/RP> \t\t\t\t\t\t-- Types of contacts at each time step: CP (contact process) or RP (reactive process). This parameter is only used in the DTMC SyncMC method." << endl;

     
     cout << endl << " Dynamical parameters (both for CTMC and DTMC):" << endl;
     cout << "   -lambda <value> \t\t-- Inter-layer spreading rate." << endl;
     cout << "   -delta <value> \t\t-- Recovery rate." << endl;
     cout << "   -eta  <value> \t\t-- Intra-layer spreading rate." << endl;
     cout << "   -y0 <value> \t\t\t-- Initial fraction of Spreaders." << endl;
     cout << "   -tmax <value> \t\t-- Maximum time, needed for CTMC Single-run and DTMC methods. This parameter does not apply to QS methods." << endl;

     
     cout << endl << " Dynamical parameters (only for CTMC):" << endl;
     cout << "   -nu <value> \t\t\t-- Intra-layer stifling rate (only for the MK Dynamical process and only implemented in its CTMC version)." << endl;
     cout << "   -alpha <value> \t\t-- The shape parameter for the Weibull distribution. If \\alpha = 1.0 (standard) the exponential distribution is recovered [Markovian]." << endl;
     cout << "   -Id <string> \t\t-- External parameter to create multiple simulations with the same parameters (standard value = 0)." << endl;
     
     cout << endl << " Dynamical parameters (only for DTMC):" << endl;
     cout << "   -Reinfection \t\t-- This parameter allows for the re-infections, i.e. an infected node can recover and be (re)infected at the same time step." << endl;
     cout << "   -gamma <value> \t\t-- Number of contacts of active individual preform at each time step for: -Method SyncMarkov. If \\gamma > 10^4 we have a RP and \\gamma = 1 we have a CP." << endl;
     cout << "   -Nexp <value> \t\t-- Number of independent Monte Carlo simulations for: -Method SyncMC." << endl;
          
     cout << endl << " Adaptative Quasi Stationary method (QS) parameters (only for CTMC):" << endl;
     cout << "   -QStr <value> \t\t-- Relaxation time." << endl;
     cout << "   -QSta <value> \t\t-- Sampling time for the . The smapling time will be \"t >= 2*QStr + k_QSta*QSta\", where at every QSta passes the statistics are computed and if the calculated is not" << endl; 
     cout << "\t\t\t\t   susceptibility difference between two time windows are less than 0.0001, then the algorithm stops. k_QSta <= 100, thus, if the susceptibility difference, 0.0001, " << endl; 
     cout << "\t\t\t\t   reached, the algorithm stops at  QSta = 100." << endl;
     cout << "   -QSp <value> \t\t-- Updating list probability." << endl;
     cout << "   -QSm <value> \t\t-- Size of the list of active visited micro-states (standard M = 25)." << endl;
     
     cout << "   -parameters <file name> \t-- Input parameter file. If -Method QS lambda, the file will be interpreted as the values of \\lambda to be evaluated, if -Method QS eta, the file will be interpreted as" << endl;
     cout << "\t\t\t\t   the values of \\eta to be evaluated. The first line of the file has the number of parameters to be evaluated, N_lines, and the following N_lines are the parameters." << endl;
     
     cout << endl << "   -refs \t\t\t-- Show the related references." << endl;
     cout << endl << "   -help \t\t\t-- Show this help and exit." << endl;
     
     cout << endl << "Examples:" << endl;
     
     cout << " 1) QS Examples:" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -eta 0.01 -y0 0.01 -alpha 1.0 -QStr 25000 -QSta 100 -QSp 0.01 -QSm 25 -Time CTMC -Process SIS -Method QS lambda -parameters lambda.txt" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.01 -y0 0.01 -alpha 1.0 -QStr 30000 -QSta 1000 -QSp 0.01 -QSm 25 -Time CTMC -Process SIS -Method QS eta -parameters eta.txt" << endl;
     
     
     cout << " 2) CTMC Single run Examples:" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.3 -eta 0.1 -y0 0.01 -alpha 1.0 -tmax 20  -Time CTMC -Process SIS -Method SingleRun -Id 00" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.3 -eta 0.1 -y0 0.01 -alpha 1.0 -tmax 20  -Time CTMC -Process SIR -Method SingleRun -Id 00" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 1.0 -eta 0.5 -nu 0.5 -y0 0.01 -alpha 1.0 -tmax 20  -Time CTMC -Process MK -Method SingleRun -Id 00" << endl;
     
     
     cout << " 3) Cellular Automata Examples:" << endl;
     cout << "  3.a) Monte Carlo Simulations:" << endl;
     
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -Contacts RP -Process SIS -Method SyncMC -Nexp 100" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -Contacts RP -Reinfection -Process SIS -Method SyncMC -Nexp 100" << endl;
     
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 0.1 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -Contacts CP -Process SIS -Method SyncMC -Nexp 100" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 0.1 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -Contacts CP -Reinfection -Process SIS -Method SyncMC  -Nexp 100" << endl;

     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -Contacts RP -Process SIR -Method SyncMC -Nexp 100" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 0.1 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -Contacts CP -Process SIR -Method SyncMC -Nexp 100" << endl;

     cout << "  3.b) Markov chain iterative equations:" << endl;
     
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -gamma 100000 -Process SIS -Method SyncMarkov" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -gamma 100000 -Reinfection -Process SIS -Method SyncMarkov" << endl;
     
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 0.1 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -gamma 1 -Process SIS -Method SyncMarkov" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 0.1 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -gamma 1 -Reinfection -Process SIS -Method SyncMarkov" << endl;

     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 1.0 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -gamma 100000 -Process SIR -Method SyncMarkov" << endl;
     cout << "   spreadingmultilayer -network 1kMuxER1030.edgelist -delta 0.1 -lambda 0.15 -eta 0.01 -y0 0.01 -tmax 1000 -Time DTMC -gamma 1 -Process SIR -Method SyncMarkov" << endl;

     
     cout << endl;
     PrintRefs();
     
     cout << endl;
     PrintExtra();
     cout << endl;
     
     return 0;
}


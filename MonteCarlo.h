#ifndef MonteCarlo_H
#define MonteCarlo_H

#include "Defines.h"

#include "Multilayer.h"

#include <stdlib.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#include <gsl/gsl_statistics_double.h>


typedef struct {
    long *I, *R, *S;
    } sir_rates;

    
sir_rates GetEvolution ( char* evolution, int steps, long N );
void FreeRateSIR ( sir_rates *R );

sir_rates* GetEvolutionLayer ( char* evolution, int steps, long N, long L );
void FreeRateSIRLayer ( sir_rates *R, long L );

char* MonteCarlo_SIS_RP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic );
char* MonteCarlo_SIS_RP_Reinfection ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic );

char* MonteCarlo_SIR_RP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic );

char* MonteCarlo_SIS_CP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic );
char* MonteCarlo_SIS_CP_Reinfection ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic );

char* MonteCarlo_SIR_CP ( Multilayer &M, int steps, double lambda, double eta, double delta, const char* initial, gsl_rng * randomic );

void MonteCarlo_Aux ( Multilayer &Ml, int Nexp, double y0, int steps, double lambda, double eta, double delta, gsl_rng * randomic, int type, char *fname );


#endif

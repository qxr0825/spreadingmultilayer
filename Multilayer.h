#ifndef Multilayer_H
#define Multilayer_H

#include <iostream>
#include <vector>
#include <utility>
#include <cmath>
#include <fstream>
#include <string.h>

#include <ctime>
#include <unistd.h>

using namespace std;

typedef pair <long,long> Edge;
typedef pair <long,long> Vertex;

typedef pair <Vertex,Vertex> Full_Edge;
typedef vector <Full_Edge>  EdgelList;

typedef vector <vector <Edge> > AdjList;
typedef vector < AdjList > Multilayer_AL;

typedef vector < long > Degree;
typedef vector < Degree > DegreeL;

typedef vector <vector <long> > IdList;
typedef vector < IdList > Multilayer_IdL;

class Multilayer {
    public:
        Multilayer_AL ML;
        EdgelList EL;
        Multilayer_IdL IL;

        long N, M, E, interE;

        long type;

        DegreeL kIntra;
        DegreeL kInter;

        Multilayer();
        Multilayer ( long type );
        ~Multilayer();

        long addLayer() {
            ML.push_back ( AdjList() );
            M++;

            if ( type == 2 ) {
                IL.push_back ( IdList() );
                }

            return M-1;
            }

        long addVertice ( ) {
            for ( long i=0; i<M; i++ ) {
                ML[i].push_back ( vector <Edge>() );
                }
            N++;

            if ( type == 2 ) {
                for ( long i=0; i<M; i++ ) {
                    IL[i].push_back ( vector <long>() );
                    }
                }

            return N-1;
            }

        long addEdge ( long vin, long vout, long Min, long Mout ) {
            if ( type == 2 ) {
                // EdgelList
                Vertex Vin = make_pair ( vin, Min );
                Vertex Vout = make_pair ( vout, Mout );

                EL.push_back ( make_pair ( Vin, Vout ) );

                if ( Min != Mout ) {
                    interE++;
                    }

                // Adjacency list (standard)
                ML[Mout][vout].push_back ( make_pair ( vin, Min ) );
                IL[Mout][vout].push_back ( E );

                E++;

                }
            else if ( type == 1 ) {   // EdgelList
                Vertex Vin = make_pair ( vin, Min );
                Vertex Vout = make_pair ( vout, Mout );

                EL.push_back ( make_pair ( Vin, Vout ) );

                E++;

                if ( Min != Mout ) {
                    interE++;
                    }

                }
            else {   // Adjacency list (standard)
                ML[Mout][vout].push_back ( make_pair ( vin, Min ) );
                E++;

                if ( Min != Mout ) {
                    interE++;
                    }

                }

            return E-1;
            }

        void ReadEdgelist ( string fname ) {
            ifstream ifs ( fname.c_str(), ifstream::in );

            long Ni, Mi, Ei;

            ifs >> Ni;
            ifs >> Mi;
            ifs >> Ei;

            for ( long i=0; i<Mi; i++ ) {
                addLayer();
                }

            for ( long j=0; j<Ni; j++ ) {
                addVertice ( );
                }


            for ( long i=0; i<Ei; i++ ) {
                long vin, vout, Min, Mout;

                ifs >> vout;
                ifs >> vin;
                ifs >> Mout;
                ifs >> Min;

                addEdge ( vin, vout, Min, Mout );
                addEdge ( vout, vin, Mout, Min );
                }

            ifs.close();
            }



        void imprime ( ostream &out ) {
            for ( long i=0; i< ( long ) ML.size(); i++ ) {
                for ( long j=0; j< ( long ) ML[i].size(); j++ )
                    for ( long k=0; k< ( long ) ML[i][j].size(); k++ ) {
                        out << j << "\t" << ML[i][j][k].first << "\t" << i << "\t" << ML[i][j][k].second << endl;
                        }
                }
            }


        void DegreeCentrality() {
            for ( long i=0; i<M; i++ ) {
                kIntra.push_back ( Degree() );
                kInter.push_back ( Degree() );

                for ( long j=0; j<N; j++ ) {
                    kIntra[i].push_back ( 0 );
                    kInter[i].push_back ( 0 );
                    }
                }


            if ( this->type == 1 ) {
                for ( long i=0; i< ( long ) EL.size(); i++ ) {
                    if ( EL[i].first.second == EL[i].second.second ) { // same layer
                        kIntra[EL[i].first.second][EL[i].first.first]++;
                        }
                    else {
                        kInter[EL[i].first.second][EL[i].first.first]++;
                        }
                    }


                }
            else {
                for ( long i=0; i<M; i++ ) {
                    for ( long j=0; j<N; j++ ) {
                        for ( long k=0; k< ( long ) ML[i][j].size(); k++ ) {
                            if ( ML[i][j][k].second == i ) {
                                kIntra[i][ML[i][j][k].first]++;


                                }
                            else {
                                kInter[i][j]++;


                                }
                            }
                        }
                    }
                }
            }

    };


#endif

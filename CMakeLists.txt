cmake_minimum_required(VERSION 3.0)

project(spreadingmultilayer)

set(SRC main.cpp Multilayer.cpp ContinuousProcesses.cpp MonteCarlo.cpp Markov.cpp QS.cpp Help.cpp)

set(CMAKE_CXX_FLAGS "-O2 -pipe -Wall -funroll-loops -lgsl -lgslcblas -fopenmp -lpthread -static")

add_executable(spreadingmultilayer  ${SRC})

target_link_libraries(spreadingmultilayer gsl pthread)


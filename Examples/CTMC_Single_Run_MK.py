# This example shows how to obtain the temporal behavior of an MK dynamics in
# the continuous-time formalism. For more on this, please see:
#   [1] G. F. de Arruda, F. A. Rodrigues, and Y. Moreno, Physics Reports 756, 1 (2018).
#      https://doi.org/10.1016/j.physrep.2018.06.007
#
# WARNING: This script is time costly. Consider running the variables cmd before
# running this script. 
# NOTE: Before runnig the simulations we check if the output file already exists


import numpy as np
from matplotlib import pyplot as plt  
import os.path

import matplotlib
matplotlib.use("pdf")


plt.rcParams['text.usetex'] = True
plt.rcParams['text.latex.unicode'] = True
plt.rcParams.update({'font.size': 18})

plt.ion();

fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, figsize=(8,10))


# Number of nodes
N = 1000;

# Defining the parameters
param_file_name = "1kMuxER1030.edgelist";
param_lambda = 1.0;
param_eta = 0.5;
param_delta = 1.0;
param_nu = 0.5;
param_alpha = 1.0;
param_tmax = 15;
param_y0 = 0.010000;
param_ID = "0";

Nexp = 50;

Tmax = 0.0;

os.chdir("./Data_MK/");
for i in range(Nexp):
    fname = "./SingleRun_MK_%s_lambda_%f_eta_%f_nu_%6f_delta_%f_tmax_%f_alpha_%f_y0_%f_ID_%d.txt" % (param_file_name, param_lambda, param_eta, param_nu, param_delta, param_tmax, param_alpha, param_y0, i);
    
    if os.path.isfile( fname ) == False:
        cmd = "./spreadingmultilayer -network " + param_file_name + " -lambda " + str(param_lambda) + " -delta " + str(param_delta) + " -eta " + str(param_eta) + " -nu " + str(param_nu) + " -y0 " + str(param_y0) + " -alpha " + str(param_alpha) + " -tmax " + str(param_tmax) + " -Time CTMC -Process MK -Method SingleRun -Id " + str(i);
        
        os.system(cmd);
    
    Data = np.loadtxt( fname , delimiter='\t');
    
    Tmax = np.max([Tmax, np.max(Data[:,0])]);

    if i == 0:
    #    ax1.plot(Data[:,0], Data[:,1]/(2*N), '-r', alpha=0.45, label=r'$X$');
        ax1.plot(Data[:,0], Data[:,2]/(2*N), '-r', alpha=0.45, label=r'$Y$');
        ax1.plot(Data[:,0], Data[:,3]/(2*N), '-b', alpha=0.45, label=r'$Z$');
        
        ax2.plot(Data[:,0], Data[:,4]/N, '-g', alpha=0.45, label=r'$Y^0$');
    #    ax2.plot(Data[:,0], Data[:,5]/N, '-r', alpha=0.45, label=r'$X^0$');
        ax2.plot(Data[:,0], Data[:,6]/N, '-r', alpha=0.45, label=r'$Z^0$');
        
        ax2.plot(Data[:,0], Data[:,7]/N, '-b', alpha=0.45, label=r'$Y^1$');
    #    ax2.plot(Data[:,0], Data[:,8]/N, '-r', alpha=0.45, label=r'$X^1$');
        ax2.plot(Data[:,0], Data[:,9]/N, '-k', alpha=0.45, label=r'$Z^1$');
    else:
    #    ax1.plot(Data[:,0], Data[:,1]/(2*N), '-r', alpha=0.45);
        ax1.plot(Data[:,0], Data[:,2]/(2*N), '-r', alpha=0.45);
        ax1.plot(Data[:,0], Data[:,3]/(2*N), '-b', alpha=0.45);
        
        ax2.plot(Data[:,0], Data[:,4]/N, '-g', alpha=0.45);
    #    ax2.plot(Data[:,0], Data[:,5]/N, '-r', alpha=0.45);
        ax2.plot(Data[:,0], Data[:,6]/N, '-r', alpha=0.45);
        
        ax2.plot(Data[:,0], Data[:,7]/N, '-b', alpha=0.45);
    #    ax2.plot(Data[:,0], Data[:,8]/N, '-r', alpha=0.45);
        ax2.plot(Data[:,0], Data[:,9]/N, '-k', alpha=0.45);

    



ax1.set_ylabel(r'$\rho$')

ax2.set_ylabel(r'$\rho$')
ax2.set_xlabel(r'$time$')

ax2.set_xlim([0, Tmax]);

ax1.legend(loc=4, fancybox=False, edgecolor='k', fontsize=24)
ax2.legend(loc=4, fancybox=False, edgecolor='k', fontsize=24)

plt.tight_layout();

os.chdir("../");
plt.savefig("./Example_CTMC_MK.pdf");
